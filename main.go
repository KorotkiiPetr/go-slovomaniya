package main

import (
	"go-slovomaniya/listwords"
	"go-slovomaniya/web"
)

func main() {
	listwords.LoadWords()
	web.Start()
}
