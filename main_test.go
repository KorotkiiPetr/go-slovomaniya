package main

import (
	"fmt"
	"testing"

	"go-slovomaniya/calculation"
	"go-slovomaniya/listwords"
)

func TestLoad(t *testing.T) {
	listwords.LoadWords()
}

func TestAnswer(t *testing.T) {
	s := calculation.GetAllCombinations(`нвралоуизвокйнрг`)
	fmt.Println(s)
}
