package web

import (
	"fmt"
	"net/http"
)

func Start() {
	http.HandleFunc("/", Site)
	http.HandleFunc("/answer", GetABC)
	fmt.Println("Server start...")
	http.ListenAndServe(":8080", nil)
}
