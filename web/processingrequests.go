package web

import (
	"net/http"

	"go-slovomaniya/calculation"
)

func GetABC(w http.ResponseWriter, r *http.Request) {
	question := r.URL.Query().Get("an")
	if question == "" {
		w.Write([]byte("Вы не ввели буквы!"))
		return
	}
	if len([]rune(question)) != 16 {
		w.Write([]byte("Вы ввели не 16 символов!"))
		return
	}
	answerMas := calculation.GetAllCombinations(question)
	var answer string
	for _, val := range answerMas {
		if answer == "" {
			answer += val
		} else {
			answer += " ," + val
		}
	}
	w.Write([]byte(answer))
}
