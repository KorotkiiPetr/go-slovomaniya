package web

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

var SourceSite []byte

func Site(w http.ResponseWriter, r *http.Request) {
	w.Write(SourceSite)
}

func init() {
	var err error
	SourceSite, err = ioutil.ReadFile("source.html")
	if err != nil {
		fmt.Println(err)
	}
}
