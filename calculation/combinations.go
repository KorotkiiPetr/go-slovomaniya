package calculation

import (
	"go-slovomaniya/listwords"
	"sync"
)

var WordsFound []string

type OptionsStep struct {
	Steps []int
}

type AllSteps []*OptionsStep

//Variants of movement around a letter by indices.
var allSteps AllSteps

//Created steps
func init() {
	step := []int{1, 4, 5}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{0, 2, 4, 5, 6}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{1, 3, 5, 6, 7}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{2, 6, 7}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{0, 1, 5, 8, 9}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{0, 1, 2, 4, 6, 8, 9, 10}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{1, 2, 3, 5, 7, 9, 10, 11}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{2, 3, 6, 10, 11}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{4, 5, 9, 12, 13}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{4, 5, 6, 8, 10, 12, 13, 14}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{5, 6, 7, 9, 11, 13, 14, 15}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{6, 7, 10, 14, 15}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{8, 9, 13}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{8, 9, 10, 12, 14}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{9, 10, 11, 13, 15}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
	step = []int{10, 11, 14}
	allSteps = append(allSteps, &OptionsStep{
		Steps: step,
	})
}

//Symbols are located by location index in the game
var char [16]string

func GetAllCombinations(inWebChar string) []string {
	for i := 0; i < 16; i++ {
		char[i] = string([]rune(inWebChar)[i])
	}
	var gone []int
	wb := &sync.WaitGroup{}
	for i := 0; i < 16; i++ {
		wb.Add(1)
		go goGetComb(gone, i, wb)
	}
	wb.Wait()
	return WordsFound
}

func goGetComb(gone []int, used int, wb *sync.WaitGroup) {
	defer wb.Done()
	getCombinations(gone, used)
}

func getCombinations(gone []int, used int) {
	possibleStep := allSteps[used].Steps
	gone = append(gone, used)
	str := ""
	if len(gone) > 2 {
		for _, val := range gone {
			str += char[val]
		}
	}
	if searchPieceOfWord(str) {
		for _, step := range possibleStep {
			continueFlag := false
			for _, val := range gone {
				if step == val {
					continueFlag = true
					break
				}
			}
			if continueFlag {
				continue
			}
			getCombinations(gone, step)
		}
	}
}

func searchPieceOfWord(comb string) (search bool) {
	if len([]rune(comb)) == 0 {
		return true
	}
	sizeComb := len([]rune(comb))
	start, end := listwords.SaerchABC(string(([]rune(comb)[0])) + string(([]rune(comb)[1])) + string(([]rune(comb)[2])))
	if start == -1 {
		return false
	}
	list := listwords.ListWords[start:end]
	for i, word := range list {
		if len([]rune(word)) < sizeComb {
			continue
		}
		if comb == string([]rune(word)[:sizeComb]) {
			matcheAdd(comb, list[i:])
			return true
		}
	}
	return false
}

func matcheAdd(comb string, list []string) {
	for _, word := range list {
		if string([]rune(comb)[0]) != string([]rune(word)[0]) {
			return
		}
		if comb == word {
			for _, wordfound := range WordsFound {
				if comb == wordfound {
					return
				}
			}
			WordsFound = append(WordsFound, comb)
			return
		}
	}
}
